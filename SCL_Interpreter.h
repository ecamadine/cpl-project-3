/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 3rd Deliverable

	SCL_Interpreter.h

	Purpose: Defines the class that handles the SCL Interpreter
*/

#ifndef SCL_INTERPRETER_ALREADYINCLUDED
#define SCL_INTERPRETER_ALREADYINCLUDED 1

class SCL_Interpreter
{
	public:

		void ClassInitializationRoutine();
		void Start();
		void Finish();

		void SetScanner(SCL_Scanner* Scanner)
		{
			this->Scanner = Scanner;
		}

		unsigned int Dispatch(Node* entry);

	private:

		bool Implementations;
		bool FunctionDefined;
		bool Variables;

		SCL_Scanner* Scanner;
};

#endif