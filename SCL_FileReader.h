/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 3rd Deliverable

	SCL_FileReader.h
	
	Purpose: Defines the class that handles the file reader.
*/

#ifndef SCL_FILEREADER_ALREADYINCLUDED
#define SCL_FILEREADER_ALREADYINCLUDED 1

class SCL_FileReader
{
	public:
	
		SCL_FileReader(string FileName, int Flags);
		~SCL_FileReader();
		
		bool CloseFile();
		bool IsFileOpen();
		bool OpenFile(string FileName, int Flags);
		bool ReadLine(string& Line);
		bool ReadLine(string& Line, char Delim);
		
		streampos GetFileSize();

		string ReturnFileName()
		{
			return FileName;
		}
	
	private:
				
		ifstream File;
		
		streampos FileBegin, FileEnd, FileSize;

		string FileName;
};

#endif