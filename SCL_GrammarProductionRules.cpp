/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Student Name:       Miles Hollis
	Student Email:      mholli10@students.kennesaw.edu
	Assignment:			Term Project 3rd Deliverable

	SCL_GrammarProductionRules.cpp
	
	Purpose: Implements the methods of the class SCL_GrammarProductionRules.
*/

#include "RequiredHeaders.h"

using namespace std;

#include "CommonComponentInterface.h"

#include "SCL_FileReader.h"

#include "SCL_Helpers.h"
#include "SCL_Driver.h"
#include "SCL_Keywords.h"
#include "SCL_Scanner.h"
#include "SCL_Parser.h"
#include "SCL_GrammarProductionRules.h"
#include "SCL_Symbols.h"

using namespace std;

LinkedList* head;
LinkedList* tail;

void SCL_GrammarProductionRules::ClassInitializationRoutine()
{

}

void SCL_GrammarProductionRules::Start()
{
	Parser->SetState(Production_Rule_Start);

	head = new LinkedList(nullptr);
	tail = head;
}

void SCL_GrammarProductionRules::Finish()
{

}

Node* SCL_GrammarProductionRules::const_var_struct()
{
	cout << "<const_var_struct>" << endl;

	Node* node = var_dec();

	return node;
}

Node* SCL_GrammarProductionRules::pother_oper_def()
{
	cout << "<pother_oper_def>" << endl;

	Node* node = new Node(Scanner->GetCurrentToken());
	node->SetLabel("<pother_oper_def>");

	int current_token = Scanner->GetCurrentToken();

	if(current_token == Identifier || current_token == MAIN)
	{
		Scanner->AdvanceToNextToken();

		current_token = Scanner->GetCurrentToken();

		if(current_token == PARAMETERS)
		{
			cout << "parameters" << endl;

			// not implemented
		}
		else
		{
			cout << "no parameters" << endl;
		}

		if(current_token == IS)
		{
			Scanner->AdvanceToNextToken();

//			fix
			const_var_struct();

//			Scanner->precond()

			current_token = Scanner->GetCurrentToken();

			if(current_token == BEGIN)
			{
				Node* new_node = new Node(Scanner->GetCurrentToken());
				new_node->SetLabel("<begin>");
				node->add(new_node);

				cout << "<begin>" << endl;

				Scanner->AdvanceToNextToken();

				current_token = Scanner->GetCurrentToken();

				while(current_token != ENDFUN)
				{
					new_node = action_def(current_token);

					current_token = Scanner->GetCurrentToken();

					tail = tail->Insert(new_node);
				}

				new_node = endfun();


				node->add(new_node);
			}
		}
	}

	return node;
}

Node* SCL_GrammarProductionRules::funct_body()
{
	cout << "<funct_body>" << endl;

	Node* node = new Node(Scanner->GetCurrentToken());
	node->SetLabel("<funct_body>");

	int current_token = Scanner->GetCurrentToken();

	if(current_token == FUNCTION)
	{
		Scanner->AdvanceToNextToken();

//		phead_func()

		Node* new_node = pother_oper_def();
		node->add(new_node);
	}
	
	return node;
}

Node* SCL_GrammarProductionRules::funct_list()
{
	cout << "<funct_list>" << endl;

	Node* node = new Node(Scanner->GetCurrentToken());
	node->SetLabel("<funct_list>");

	Node* new_node = funct_body();

	node->add(new_node);

	return node;
}

Node* SCL_GrammarProductionRules::main_head()
{
	cout << "<main_head>" << endl;

	Node* node = new Node(Scanner->GetCurrentToken());
	node->SetLabel("<main_head>");

	Node* new_node = funct_list();

	node->add(new_node);

	return node;
}

Node* SCL_GrammarProductionRules::implement()
{
	cout << "<implement>" << endl;

	Node* node = new Node(Scanner->GetCurrentToken());
	node->SetLabel("<implement>");
	tail = tail->Insert(node);

	Node* new_node = main_head();
	node->add(new_node);

	// adjust return type?

	return node;
}

Node* SCL_GrammarProductionRules::globals()
{
	cout << "<globals>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == GLOBAL)
	{
		node = new Node(Scanner->GetCurrentToken());
		node->SetLabel("<globals>");
		tail = tail->Insert(node);

		Scanner->AdvanceToNextToken();

		const_dec();
		var_dec();
//		Scanner->struct_dec
	}

	return node;
}

Node* SCL_GrammarProductionRules::import()
{
	cout << "import" << endl;

	Node* node = new Node(Scanner->GetCurrentToken());

	tail = tail->Insert(node);

	Scanner->AdvanceToNextToken();

	Node* lexeme_node = new Node(Scanner->GetCurrentLexeme(), Scanner->GetCurrentToken());
	lexeme_node->SetLabel(Scanner->GetCurrentLexeme());

	node->SetLabel("<import>");
	node->add(lexeme_node);

	Scanner->AdvanceToNextToken();

	return node;
}
	
Node* SCL_GrammarProductionRules::stage(int Token)
{
	Node* node = nullptr;

	if(Token == IMPORT)
	{
		node = import();
	}

	if(Token == IMPLEMENTATIONS)
	{
		Scanner->AdvanceToNextToken();

		node = implement();

		if(Token == GLOBAL)
		{
			node = globals();

			if(Parser->GetState() != Production_Rule_Error)
			{
				Parser->SetState(Production_Rule_End);
				cout << "Successfully parsed expressions." << endl;
			}
			else
			{
				cout << "Had error while parsing. Attempting to recover." << endl;
			}

		}
	}

	return node;
}

// wraps the action defintion

Node* SCL_GrammarProductionRules::pactions(int Token)
{
	Node* node = nullptr;

	cout << "pactions" << endl;
	
	node = action_def(Token);
	
	return node;
}

// action defintion, handles a significant majority of 
// grammar rules that we need to have for production

Node* SCL_GrammarProductionRules::action_def(int Token)
{
	cout << "<action_def>" << endl;

	Node* node = nullptr;

	switch (Token)
	{
		case SET:
		{
			node = set();
		}
		break;

		case READ:
		{
			node = pvar_value_list();
		}
		break;

		case INPUT:
		{
			cout << "input" << endl;
		}
		break;

		case DISPLAY:
		{
			node = display();
		}
		break;

		case DISPLAYN:
		{
			node = displayn();
		}
		break;

		case MCLOSE:
		{
			cout << "mclose" << endl;
		}
		break;

		case MFILE:
		{
			cout << "mfile" << endl;
		}
		break;

		case INCREMENT:
		{
//			node = increment();
			// name_ref();
		}
		break;

		case DECREMENT:
		{
//			node = decrement();
			// name_ref();
		}
		break;

		case RETURN:
		{
			node = _return();
		}
		break;

		case CALL:
		{
			node = call();
		}
		break;

		case IF:
		{
			cout << "if" << endl;
		}
		break;

		case FOR:
		{
			cout << "for" << endl;
		}
		break;

		case WHILE:
		{
			cout << "while" << endl;
		}
		break;

		case CASE:
		{
			cout << "case" << endl;
		}
		break;

		case MBREAK:
		{
			cout << "mbreak" << endl;
		}
		break;

		case MEXIT:
		{
			cout << "mexit" << endl;
		}
		break;

		case ENDFUN:
		{
			node = endfun();
		}
		break;

		case EXIT:
		{
			node = exit();
		}
		break;

		case POSTCONDITION:
		{
			cout << "postconditon" << endl;
		}
		break;
	}

	return node;
}


// exit, defines the exit point for a routine
Node* SCL_GrammarProductionRules::exit()
{
	cout << "<exit>" << endl;

	Node* node = new Node(Scanner->GetCurrentToken());

	node->SetLabel("<exit>");

	Scanner->AdvanceToNextToken();

	return node;
}

// end function, defines the end point for a function block
Node* SCL_GrammarProductionRules::endfun()
{
	cout << "<endfun>" << endl;

	Node* node = new Node(Scanner->GetCurrentToken());
	node->SetLabel("<endfun>");

	Scanner->AdvanceToNextToken();

	int current_token = Scanner->GetCurrentToken();

	if(current_token == Identifier || current_token == MAIN)
	{
		cout << "finished parsing." << endl;

		Node* new_node = new Node(current_token);

		node->add(new_node);

		if(current_token == MAIN)
		{
			new_node->SetLabel("MAIN");
		}
		else
		{
			new_node->SetLabel(Scanner->GetCurrentLexeme());
		}
	}
	else
	{
		cout << "Missing token for identifier or main in endfun" << endl;

		Parser->SetState(Production_Rule_Error);
	}

	return node;
}

// variable declare
Node* SCL_GrammarProductionRules::var_dec()
{
	cout << "<var_dec>" << endl;

	Scanner->AdvanceToNextToken();

	int current_token = Scanner->GetCurrentToken();

	Node* variable_node = new Node(current_token);
	variable_node->SetLabel("<declare>");

	Node* node = data_declarations(variable_node);

	tail = tail->Insert(node);

	return node;
}

Node* SCL_GrammarProductionRules::call()
{
	cout << "<call>" << endl;

	int current_token = Scanner->GetCurrentToken();

	Node* call_node = new Node(current_token);
	call_node->SetLabel("<call>");

	Scanner->AdvanceToNextToken();
		
	Node* node = name_ref();
	node->SetParent(call_node);
//	# pusing_ref

	call_node->add(node);
	node->SetParent(call_node);

	tail = tail->Insert(node);

	return node;
}

Node* SCL_GrammarProductionRules::_return()
{
	cout << "<return>" << endl;

	int current_token = Scanner->GetCurrentToken();

	Node* return_node = new Node(current_token);
	return_node->SetLabel("<return>");

	Scanner->AdvanceToNextToken();

	Node* node = expr(return_node);

	return_node->add(node);
	node->SetLabel("<expr>");
	node->SetParent(return_node);

	tail = tail->Insert(node);

	return node;
}

Node* SCL_GrammarProductionRules::const_dec()
{
	cout << "<const_dec>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == CONSTANTS)
	{
		node = data_declarations(nullptr);
		node->SetLabel("<const_dec>");
		tail = tail->Insert(node);
	}
	else
	{
		cout << "Missing token for constants declaration in const_dec" << endl;

		Parser->SetState(Production_Rule_Error);
	}

	return node;
}
Node* SCL_GrammarProductionRules::iconst_ident()
{
	cout << "<iconst_ident>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == IntegerConstant || current_token == Identifier)
	{
		Scanner->AdvanceToNextToken();
	
		if(current_token == IntegerConstant)
		{
			node = new Node(Scanner->GetCurrentLexeme(), current_token);
			node->SetLabel("<constant>");
		}
		else
		{
			node = new Node(current_token);
			node->SetLabel("<identifier>");

			tail = tail->Insert(node);
		}
	}
	else
	{
		cout << "unknown token in comp_declare" << endl;

		Parser->SetState(Production_Rule_Error);
	}

	return node;
}

Node* SCL_GrammarProductionRules::data_declaration(Node* parent)
{
	cout << "<data_declaration>" << endl;

	Node* node = nullptr;
	Node* new_node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == Identifier)
	{
		Scanner->AdvanceToNextToken();

		node = new Node(current_token);
		node->SetLabel("<identifier>");

		cout << "Token_Identifier" << endl;

		if(current_token == ARRAY || current_token == LeftBracket || current_token == VALUE || current_token == Assignment)
		{
			cout << "empty case!" << endl;
		}
		else
		{
			current_token = Scanner->GetCurrentToken();

			Scanner->AdvanceToNextToken();

			if(current_token == OF)
			{
				current_token = Scanner->GetCurrentToken();

				Scanner->AdvanceToNextToken();

				if(current_token == TYPE)
				{
					current_token = Scanner->GetCurrentToken();

					Scanner->AdvanceToNextToken();

					switch (current_token)
					{
						case TUNSIGNED:
						case CHAR:
						case INTEGER:
						case DOUBLE:
						case LONG:
						case SHORT:
						case FLOAT:
						case TSTRING:
						case TBOOL:
						case TBYTE:
						{
							new_node = new Node(current_token);

//							parent->initialize(node, new_node);

							node = parent;
						}
						break;

						default:
						{
							cout << "Unsupported data type in data_declaration" << endl;

							Parser->SetState(Production_Rule_Error);
						}
						break;
					}
				}
				else
				{
					cout << "Missing TYPE in define in data_declaration" << endl;

					Parser->SetState(Production_Rule_Error);
				}
			}

			else
			{
				cout << "Missing OF in define in data_declaration" << endl;

				Parser->SetState(Production_Rule_Error);
			}

			return node;
		}
	}
}

Node* SCL_GrammarProductionRules::data_file(Node* parent)
{
	cout << "<data_file>" << endl;

	Node* node = data_declaration(parent);

	return node;
}

Node* SCL_GrammarProductionRules::comp_declare(Node* parent)
{
	cout << "<comp_declare>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == DEFINE)
	{
		Scanner->AdvanceToNextToken();

		node = data_file(parent);
	}
	else
	{
		cout << "missing define token in comp_declare" << endl;

		Parser->SetState(Production_Rule_Error);
	}

	return node;
}


// data_declarations, handles the rules for declaring variables and initializing them
Node* SCL_GrammarProductionRules::data_declarations(Node* parent)
{
	cout << "<data_declarations>" << endl;

	Node* node = comp_declare(parent);

	return node;
}

Node* SCL_GrammarProductionRules::define()
{
	cout << "<define>" << endl;

	int current_token = Scanner->GetCurrentToken();

	Node* define_node = new Node(current_token);
	define_node->SetLabel("<define>");

	Node* node = data_declarations(define_node);

	define_node->add(node);
	node->SetParent(define_node);

	return define_node;
}

// plist_const, handles the accessing of arrays

Node* SCL_GrammarProductionRules::plist_const()
{
	cout << "<plist_const>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if (current_token == LeftBracket)
	{
		Scanner->AdvanceToNextToken();

		node = iconst_ident();

		current_token = Scanner->GetCurrentToken();

		if (current_token != RightBracket)
		{
			cout << "missing right bracket in plist_const" << endl;

			Parser->SetState(Production_Rule_Error);
		}
		else
		{
			cout << "missing left bracket in plist_const" << endl;

			Parser->SetState(Production_Rule_Error);
		}

	}

	return node;
}

Node* SCL_GrammarProductionRules::opt_ref()
{
	cout << "<opt_ref>" << endl;
	Node* node = array_val();

	return node;
}

Node* SCL_GrammarProductionRules::name_ref()
{
	cout << "<name_ref>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if (current_token != Identifier)
	{
		cout << "missing identifier in name_ref" << endl;

		Parser->SetState(Production_Rule_Error);
	}
	else
	{
		node = opt_ref();
	}

	return node;
}

Node* SCL_GrammarProductionRules::display()
{
	cout << "<display>" << endl;

	Node* node = pvar_value_list();

	return node;
}

Node* SCL_GrammarProductionRules::displayn()
{
	cout << "<displayn>" << endl;

	Node* node = pvar_value_list();

	return node;
}

Node* SCL_GrammarProductionRules::pvar_value_list()
{
	cout << "<pvar_value_list>" << endl;

	// first expr

	Node* arg_list_node = new Node(Scanner->GetCurrentToken());
	arg_list_node->SetLabel("<pvar_value_list>");

	Scanner->AdvanceToNextToken();

	Node* head = expr(arg_list_node);
	head->SetParent(arg_list_node);
	arg_list_node->add(head);

	while(1)
	{
		int current_token = Scanner->GetCurrentToken();

		if(current_token != Comma)
		{
			break;
		}
		else
		{
			Scanner->AdvanceToNextToken();

			Node* node = expr(head);
			arg_list_node->add(node);
			node->SetParent(arg_list_node);
		}
	}

	return arg_list_node;
}

Node* SCL_GrammarProductionRules::arg_list()
{
	cout << "<arg_list>" << endl;

	Node* arg_list_node = new Node(Scanner->GetCurrentToken());
	arg_list_node->SetLabel("<pvar_value_list>");

	Scanner->AdvanceToNextToken();

	Node* head = expr(arg_list_node);
	head->SetParent(arg_list_node);
	arg_list_node->add(head);

	while(1)
	{
		int current_token = Scanner->GetCurrentToken();

		if (current_token != Comma)
		{
			break;
		}
		else
		{
			Scanner->AdvanceToNextToken();

			Node* node = expr(arg_list_node);
			arg_list_node->add(node);
			node->SetParent(arg_list_node);
		}
	}
	return arg_list_node;
}

Node* SCL_GrammarProductionRules::array_val()
{
	cout << "<array_val>" << endl;

	Node* node = nullptr;

	node = simp_arr_val();

	return node;
}

Node* SCL_GrammarProductionRules::simp_arr_val()
{
	cout << "<simp_arr_val>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if (current_token == LeftBracket)
	{
		node = arg_list();

		current_token = Scanner->GetCurrentToken();

		if (current_token != RightBracket)
		{
			cout << "missing right bracket in array_val" << endl;

//			Scanner->set_state(Constants.Production_Rule_Error);
		}
		else
		{
			cout << "missing left bracket in array_val" << endl;

//			Scanner->set_state(Constants.Production_Rule_Error);
		}

		return node;
	}
}

// parguments, handles the grammar rule for any rule that requires arguments, one or more

Node* SCL_GrammarProductionRules::parguments()
{
	cout << "<parguments>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == LeftParentheses)
	{
		node = arg_list();

		current_token = Scanner->GetCurrentToken();
			
		if(current_token != RightParentheses)
		{
			cout << "missing right parentheses in parguments" << endl;

			Parser->SetState(Production_Rule_Error);
		}
	}
	else
	{
		cout << "missing left parentheses in parguments" << endl;

		Parser->SetState(Production_Rule_Error);
	}

	return node;
}

		

Node* SCL_GrammarProductionRules::type_name()
{
	cout << "<type_name>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	Scanner->AdvanceToNextToken();

	switch (current_token)
	{	
		case MVOID:
		case INTEGER:
		case SHORT:
		case REAL:
		case FLOAT:
		case DOUBLE:
		case TBOOL:
		case CHAR:
		case TBYTE:
		{
			node = new Node(current_token);
		}
		break;

		case TSTRING:
		{
			Scanner->AdvanceToNextToken();

			current_token = Scanner->GetCurrentToken();

			if(current_token != OF)
			{
				cout << "missing OF in type_name" << endl;

				Parser->SetState(Production_Rule_Error);
			}
			else
			{
				if (current_token != LENGTH)
				{
					cout << "missing LENGTH in type_name" << endl;

					Parser->SetState(Production_Rule_Error);
				}
				else
				{
					node = new Node(current_token);
				}
			}
		}
	}
	
	return node;
}
Node* SCL_GrammarProductionRules::ret_type(Node* parent)
{
	cout << "<ret_type>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	Scanner->AdvanceToNextToken();

	if(current_token == TYPE)
	{
		node = type_name();
	}
	else if( current_token == STRUCT || current_token == STRUCTYPE)
	{
		current_token = Scanner->GetCurrentToken();

		Scanner->AdvanceToNextToken();

		if(current_token == IDENTIFIER)
		{
			node = new Node(current_token);
			node->SetParent(parent);
			node->SetLabel("<ret_type>");

			Parser->SetState(Production_Rule_Error);
		}
		else
		{
			cout << "missing identifier in ret_type" << endl;

			Parser->SetState(Production_Rule_Error);
		}
	}

	return node;
}
Node* SCL_GrammarProductionRules::element(Node* parent)
{
	cout << "<element>" << endl;

	Node* node = nullptr;

	bool did_match = false;

	int current_token = Scanner->GetCurrentToken();

	switch (current_token)
	{
		case Identifier:
		case String:
		case Letter:
		case IntegerConstant:
		case HexadecimalConstant:
		case FloatingPointConstant:
		case MTRUE:
		case MFALSE:
		{
			if (current_token == Identifier)
			{
				current_token = Scanner->GetCurrentToken();

				Scanner->AdvanceToNextToken();

				if (current_token == LeftParentheses)	// # parguments
				{
					node = parguments();	//  # return node
				}
				else if (current_token == LeftBracket)	// #array_val
				{
					node = array_val(); // # return node
				}
				else
				{
					node = new Node(Identifier);
					

					Node* new_node = new Node();
					node->SetParent(new_node);
					new_node->add(node);

					new_node->SetLabel("<element>");

					node = new_node;
				}
			}
			else
			{
				
				if(current_token == String)
				{
					node = new Node(Scanner->GetCurrentLexeme(), current_token);
					did_match = true;
				}
				else if(current_token == Letter)
				{
					node = new Node(Scanner->GetCurrentLexeme(), current_token);
					did_match = true;
				}
				else if(current_token == IntegerConstant)
				{
					node = new Node(std::stoi(Scanner->GetCurrentLexeme(), nullptr, 10), current_token);
					did_match = true;
				}
				else if(current_token == HexadecimalConstant)
				{
					node = new Node(std::stoi(Scanner->GetCurrentLexeme(), nullptr, 16), current_token);
					did_match = true;
				}
				else if(current_token == FloatingPointConstant)
				{
					node = new Node(std::stof(Scanner->GetCurrentLexeme(), nullptr), current_token);
					did_match = true;
				}
				else if(current_token == MTRUE or current_token == MFALSE)
				{
					node = new Node(std::stoi(Scanner->GetCurrentLexeme(), nullptr, 2), current_token);
					did_match = true;
				}
				if (did_match == true)
				{
					Node* new_node = new Node();
					node->SetParent(new_node);
					new_node->add(node);

					Scanner->AdvanceToNextToken();
					new_node->SetLabel("<element>");
					new_node->SetParent(parent);

					node = new_node;
				}
			}
		}
		break;

	}
	
	if(current_token == LeftParentheses)
	{
		node = expr(parent);

		if(current_token != RightParentheses)
		{
			cout << "missing closing right parentheses in element" << endl;

			Parser->SetState(Production_Rule_Error);
		}
		else
		{
			cout << "unknown token in element" << endl;

			Parser->SetState(Production_Rule_Error);
		}
	}

	return node;
}

Node* SCL_GrammarProductionRules::punary(Node* parent)
{
	cout << "<punary>" << endl;

	Node* node = element(parent);

	bool did_match = false;
	Node* new_node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == Minus)
	{
		new_node = new Node(Minus);
		new_node->initialize(node, element(node));
		did_match = true;
	}
	else if(current_token == NEGATE)
	{
		new_node = new Node(NEGATE);
		did_match = true;
	}
	if(did_match == true)
	{
		Scanner->AdvanceToNextToken();
		new_node->SetLabel("<punary>");
		new_node->SetParent(parent);
		return new_node;
	}
	else
	{
		new_node = new Node();
		new_node->SetLabel("<punary>");
		new_node->SetParent(parent);
		new_node->add(node);
		node = new_node;
	}

	return node;
}

Node* SCL_GrammarProductionRules::term(Node* parent)
{
	cout << "<term>" << endl;

	Node* node = punary(parent);
	bool did_match = false;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == Asterisk)
	{
		did_match = true;
	}
	else if(current_token == Division)
	{
		did_match = true;
	}
	else if(current_token == Modulus)
	{
		did_match = true;
	}
	else if(current_token == LSHIFT)
	{
		did_match = true;
	}
	else if(current_token == RSHIFT)
	{
		did_match = true;
	}
	if(did_match == true)
	{
		Node* new_node = new Node();
		Scanner->AdvanceToNextToken();
		new_node->initialize(node, punary(new_node));
		new_node->SetLabel("<term>");
		node->SetParent(new_node);

		current_token = Scanner->GetCurrentToken();

		Scanner->AdvanceToNextToken();

		if(current_token == Asterisk ||
			current_token == Division ||
			current_token == Modulus ||
			current_token == LSHIFT ||
			current_token == RSHIFT)
		{
			node = new Node();
			Node* recursive = term(parent);
			node->initialize(new_node, recursive);
			node->SetLabel("<term>");
			new_node->SetParent(node);
		}
		else
		{
			Scanner->RetractToken();
		}
		return new_node;
	}
	else
	{
		Node* new_node = new Node();
		new_node->SetLabel("<term>");
		new_node->add(node);
		node->SetParent(new_node);

		node = new_node;
	}

	return node;
}
Node* SCL_GrammarProductionRules::expr(Node* parent)
{
	cout << "<expr>" << endl;

	Node* node = term(parent);
	node->SetLabel("<expr>");

	bool did_match = false;
	Node* new_node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == Plus)
	{
		new_node = new Node(Plus);
		did_match = true;
	}
	else if(current_token == Minus)
	{
		new_node = new Node(Minus);
		did_match = true;
	}
	else if( current_token == BAND)
	{
		new_node = new Node(BAND);
		did_match = true;
	}
	else if(current_token == BOR)
	{
		new_node = new Node(BOR);
		did_match = true;
	}
	else if(current_token == BXOR)
	{
		new_node = new Node(BXOR);
		did_match = true;
	}
	else if(current_token == Exponent)
	{
		new_node = new Node(Exponent);
		did_match = true;
	}
	if(did_match == true)
	{
		Scanner->AdvanceToNextToken();
		new_node->initialize(node, term(new_node));
		new_node->SetLabel("<expr>");
		node->SetParent(new_node);

		node = new Node(current_token);

		tail = tail->Insert(node);

		return new_node;
	}

	return node;
}

Node* SCL_GrammarProductionRules::set()
{
	cout << "<set>" << endl;

	Node* set_node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	Scanner->AdvanceToNextToken();

	if(current_token == SET)
	{
		set_node = new Node(current_token);
		set_node->SetLabel("<set>");

		current_token = Scanner->GetCurrentToken();

		// if the first token after set is not an identifier, throw an error

		if(current_token != Identifier)
		{
			cout << "missing identifier in set" << endl;

//			set_state(Constants.Production_Rule_Error)
		}
		else
		{

			Node* identifier_node = new Node(Scanner->GetCurrentLexeme(), Identifier);

			Scanner->AdvanceToNextToken();

			identifier_node->SetLabel("<id>");

			set_node->SetLeft(identifier_node);
			identifier_node->SetParent(set_node);

			current_token = Scanner->GetCurrentToken();

			if(current_token != Assignment)
			{
				cout << "missing assignment token in set" << endl;

//				set_state(Constants.Production_Rule_Error)
			}
			else
			{
//				look at the first expr

				Scanner->AdvanceToNextToken();

				Node* node = expr(set_node);

				set_node->SetRight(node);
				node->SetParent(set_node);
			}
		}
	}
	else
	{
		cout << "invalid token in set" << endl;

		Parser->SetState(Production_Rule_Error);
	}

	return set_node;
}

/*
precond :: =
| PRECONDITION pcondition

pcondition :: = pcond1 OR pcond1
| pcond1 AND pcond1
| pcond1

pcond1 :: = NOT pcond2
| pcond2

pcond2 :: = LP pcondition RP
| expr RELOP expr
| expr EQOP expr
| expr eq_v expr
| expr opt_not true_false
| element

true_false :: = MTRUE
| MFALSE
eq_v :: = EQUALS
| GREATER THAN
| LESS THAN
| GREATER OR EQUAL
| LESS OR EQUAL

opt_not :: =
| NOT
*/

Node* SCL_GrammarProductionRules::precond()
{
	Node* node = nullptr;

	node = pcondition();

	return node;
}

Node* SCL_GrammarProductionRules::pcondition()
{
	cout << "<pcondition>" << endl;

	int current_token = Scanner->GetCurrentToken();

	Scanner->AdvanceToNextToken();

	Node* node_pcondition = new Node(current_token);
	node_pcondition->SetLabel("<opt_not>");

	return node_pcondition;
}

Node* SCL_GrammarProductionRules::pcond1()
{
	cout << "<pcond1>" << endl;

	int current_token = Scanner->GetCurrentToken();

	Node* pcond1 = nullptr;

	if(current_token != Not)
	{
		Node* node_not = new Node(current_token);
		node_not->SetLabel("<opt_not>");

		tail = tail->Insert(node_not);

		Node* node = pcond2(node_not);

		node_not->add(node);

		pcond1 = node_not;
	}
	else
	{
		Node* current = new Node(current_token);

		Node* node = pcond2(current);

		pcond1 = current;

		tail = tail->Insert(node);
	}

	return pcond1;
}

Node* SCL_GrammarProductionRules::pcond2(Node* parent)
{
	cout << "<pcond2>" << endl;

	Node* node = nullptr;
	Node* node_expr = nullptr;
	Node* node_expr2 = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == LeftParentheses)
	{
		Scanner->AdvanceToNextToken();

		node = pcondition();

		if(current_token != RightParentheses)
		{
			cout << "missing closing right parentheses in pcond2" << endl;

			Parser->SetState(Production_Rule_Error);
		}

//		set_state(Constants.Production_Rule_Error)
	}
	else
	{
		current_token = Scanner->GetCurrentToken();

		Scanner->AdvanceToNextToken(); // # should be on expr, if we did element then no relationals

		Node* pcond2 = new Node();
		pcond2->SetLabel("<pcond2");

		Node* node_expr = expr(pcond2);

		Scanner->AdvanceToNextToken();

		if(current_token == Relational ||
			current_token == Relational_Equals)
		{
			node_expr2 = expr(pcond2);
		}

//		tail_node->add(node_expr)
//		tail_node->add(node_expr2)


	}

	return node;
}

Node* SCL_GrammarProductionRules::true_false()
{
	cout << "<true_false>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == MTRUE ||
		current_token == MFALSE)
	{
		Scanner->AdvanceToNextToken();

		node = new Node(current_token);
		node->SetLabel("<true_false>");

		tail = tail->Insert(node);
	}
	else
	{
		cout << "missing true or false token in true_false" << endl;

		Parser->SetState(Production_Rule_Error);
	}

	return node;
}

Node* SCL_GrammarProductionRules::eq_v()
{
	cout << "<eq_v>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token == Equality ||
		current_token == Relational ||
		current_token == Relational_Equals)
	{
		Scanner->AdvanceToNextToken();

		node = new Node(current_token);
		node->SetLabel("<eq_v>");

		tail = tail->Insert(node);
	}
	else
	{
		cout << "missing equality token in opt_not" << endl;

		Parser->SetState(Production_Rule_Error);
	}

	return node;
}

Node* SCL_GrammarProductionRules::opt_not()
{
	cout << "<opt_not>" << endl;

	Node* node = nullptr;

	int current_token = Scanner->GetCurrentToken();

	if(current_token != Not)
	{
		cout << "missing not token in opt_not" << endl;

		Parser->SetState(Production_Rule_Error);
	}
	else
	{
		Scanner->AdvanceToNextToken();
	}

	node = new Node(current_token);
	node->SetLabel("<opt_not>");

	tail = tail->Insert(node);

	return node;
}