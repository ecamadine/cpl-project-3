/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 3rd Deliverable

	SCL_Scanner.cpp

	Purpose:  Implements the class methods that completes the parser class.
*/

#include "RequiredHeaders.h"

using namespace std;

#include "CommonComponentInterface.h"

#include "SCL_FileReader.h"

#include "SCL_Helpers.h"
#include "SCL_Driver.h"
#include "SCL_Keywords.h"
#include "SCL_Scanner.h"
#include "SCL_Symbols.h"
#include "SCL_GrammarProductionRules.h"
#include "SCL_Parser.h"


void SCL_Parser::ClassInitializationRoutine()
{

}

void SCL_Parser::Start()
{
	cout << "Start parsing" << endl;

	Dispatcher();
}

void SCL_Parser::Finish()
{
	cout << "Finished parsing." << endl;
}

bool SCL_Parser::Dispatcher()
{
	bool ReturnValue = true;

	size_t Index_Adjust = 0;
	size_t CurrentIndex = 0;
	int Token = 0;

	do
	{
		Token = Scanner->GetCurrentToken();

		Index_Adjust = Dispatch(Token);

		if(Index_Adjust == 0)
		{
			ReturnValue = false;

			break;
		}

		CurrentIndex += Index_Adjust;
	}while (Index_Adjust != 0);

	return ReturnValue;
}

size_t SCL_Parser::Dispatch(int Token)
{
	Scanner->SetStart();

	Rules->stage(Token);

	size_t TokensParsed = Scanner->GetNumberOfTokensParsed();

	return TokensParsed;
}