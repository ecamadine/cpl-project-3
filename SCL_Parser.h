/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 3rd Deliverable

	SCL_Parser.h

	Purpose: Defines the parser class that completes the assignment guidelines
*/

#ifndef SCL_PARSER_ALREADYINCLUDED
#define SCL_PARSER_ALREADYINCLUDED 1

class SCL_GrammarProductionRules;

class SCL_Parser : public CommonComponentInterface
{
public:

	bool Dispatcher();
	size_t Dispatch(int Token);
	
	void ClassInitializationRoutine();
	void Start();
	void Finish();

	void SetScanner(SCL_Scanner* Scanner)
	{
		this->Scanner = Scanner;
	}

	void SetRules(SCL_GrammarProductionRules* Rules)
	{
		this->Rules = Rules;
	}

	int GetState(){ return State; } 
	void SetState(int NewState)
	{ 
		if ( NewState >= Production_Rule_Start && NewState <= Production_Rule_End )
		{
			State = NewState;
		}
		else
		{
			cout << "Invalid state." << endl;
		}
	}

private:

	int State;

	SCL_Scanner* Scanner;
	SCL_GrammarProductionRules* Rules;
};

#endif