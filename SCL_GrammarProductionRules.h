/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 3rd Deliverable

	SCL_GrammarProductionRules.h

	Purpose: Defines the production rules used by the recursive-descent parsing
*/

#ifndef SCL_GRAMMARPRODUCTIONRULES_ALREADYINCLUDED
#define SCL_GRAMMARPRODUCTIONRULES_ALREADYINCLUDED 1

class Node
{
	public:

		Node()
		{
		}

		Node(int token)
		{
			this->token = token;
		}

		Node(int value, int token)
		{
			this->integer_value = value;
			this->token = token;
		}

		Node(float value, int token)
		{
			this->float_value = value;
			this->token = token;
		}

		Node(string& lexeme, int token)
		{
			this->lexeme = lexeme;
			this->token = token;
		}

		Node(string lexeme, int token)
		{
			this->lexeme = lexeme;
			this->token = token;
		}

		Node(string& label, string& lexeme, int token)
		{
			this->token = token;
			this->label = label;
			this->lexeme = lexeme;
		}

		Node(string label, string lexeme, int token)
		{
			this->token = token;
			this->label = label;
			this->lexeme = lexeme;
		}

		Node* GetParent()
		{
			return parent;
		}

		Node* GetLeft()
		{
			return left;
		}

		Node* GetRight()
		{
			return right;
		}

		void SetLeft(Node* left)
		{
			this->left = left;
		}

		void SetParent(Node* parent)
		{
			this->parent = parent;
		}

		void SetRight(Node* right)
		{
			this->right = right;
		}

		string GetLexeme()
		{
			return lexeme;
		}

		string GetLabel()
		{
			return label;
		}

		int GetToken()
		{
			return token;
		}

		void SetLabel(string& label)
		{
			this->label = label;
		}

		void SetLabel(string label)
		{
			this->label = label;
		}

		void SetLabel(char* label)
		{
			this->label = label;
		}

		void SetLabel(const char* label)
		{
			this->label = label;
		}

		void SetLexeme(string& lexeme)
		{
			this->lexeme = lexeme;
		}
	
		void SetNext(Node* next)
		{
			this->next = next;
		}
		
		void SetToken(int token)
		{
			this->token = token;
		}

		void add(Node* node)
		{
			if(left == nullptr)
			{
				left = node;
			}
			else
			{
				right = node;
			}
		}

		void initialize(Node* left, Node* right)
		{
			this->left = left;
			this->right = right;
		}

	private:

		Node* next;
		Node* parent;
		Node* left;
		Node* right;

		int token;
		string lexeme;
		string label;

		union
		{
			char char_value;
			short short_value;
			long long_value;
			char* char_pointer_value;
			int integer_value;
			float float_value;
			double double_value;
			string string_value;
		};
};

class LinkedList
{
	public:

		LinkedList(void* Data)
		{
			this->Data = Data;
			next_node = nullptr;
		}

		LinkedList* GetNext()
		{
			return next_node;
		}

		void SetNext(LinkedList* new_node)
		{
			next_node = new_node;
		}

		void* GetData()
		{
			return Data;
		}

		LinkedList* Insert(void* Data)
		{
			LinkedList* new_node = new LinkedList(Data);
			SetNext(new_node);
			return new_node;
		}

	private:
		LinkedList* next_node;

		void* Data;
};

extern LinkedList* head;
extern LinkedList* tail;

class SCL_GrammarProductionRules : public CommonComponentInterface
{
public:

//	SCL_GrammarProductionRules();
//	~SCL_GrammarProductionRules();

	void ClassInitializationRoutine();
	void Start();
	void Finish();

	Node* const_var_struct();
	Node* pother_oper_def();
	Node* var_dec();
	Node* action_def(int Token);

	Node* funct_body();
	Node* funct_list();
	Node* main_head();
	Node* implement();
	Node* globals();
	Node* import();
	Node* pactions(int Token);
	Node* exit();
	Node* endfun();
	Node* call();
	Node* _return();
	Node* const_dec();
	Node* iconst_ident();
	Node* data_declaration(Node* parent);
	Node* data_file(Node* parent);
	Node* comp_declare(Node* parent);
	Node* data_declarations(Node* parent);
	Node* define();
	Node* plist_const();
	Node* opt_ref();
	Node* name_ref();
	Node* display();
	Node* displayn();
	Node* pvar_value_list();
	Node* arg_list();
	Node* array_val();
	Node* simp_arr_val();
	Node* parguments();
	Node* type_name();
	Node* ret_type(Node* parent);
	Node* element(Node* parent);
	Node* punary(Node* parent);
	Node* term(Node* parent);
	Node* expr(Node* parent);
	Node* set();
	Node* precond();
	Node* pcondition();
	Node* pcond1();
	Node* pcond2(Node* parent);

	Node* true_false();
	Node* eq_v();
	Node* opt_not();

	Node* stage(int Token);

	void SetScanner(SCL_Scanner* Scanner)
	{
		this->Scanner = Scanner;
	}

	void SetParser(SCL_Parser* Parser)
	{
		this->Parser = Parser;
	}

private:

	SCL_Scanner* Scanner;
	SCL_Parser* Parser;
};

#endif