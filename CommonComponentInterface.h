/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 3rd Deliverable

	CommonComponentInterface.h
	
	Purpose: Defines the interface class that derived classes use.
*/

#ifndef CCI_ALREADYINCLUDED
#define CCI_ALREADYINCLUDED 1

class CommonComponentInterface
{
	public:
			
		virtual void ClassInitializationRoutine() = 0;			
		virtual void Start() = 0;
		virtual void Finish() = 0;
			
};

class SCL_Parser;
class SCL_GrammarProductionRules;

enum
{
	Production_Rule_Start = 0,
    Production_rule_Normal = 1,
    Production_Rule_Request_Token = 2,
    Production_Rule_Request_Lexeme = 3,
    Production_Rule_Error = 4,
    Production_Rule_End = 5
};

#endif