/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 3rd Deliverable

	SCL_Scanner.h

	Purpose: Implements the class methods that handles the interpreter phase.
*/

#include "RequiredHeaders.h"

using namespace std;

#include "CommonComponentInterface.h"

#include "SCL_FileReader.h"

#include "SCL_Helpers.h"
#include "SCL_Driver.h"
#include "SCL_Keywords.h"
#include "SCL_Scanner.h"
#include "SCL_Parser.h"
#include "SCL_GrammarProductionRules.h"
#include "SCL_Symbols.h"
#include "SCL_Interpreter.h"

void SCL_Interpreter::ClassInitializationRoutine()
{

}

void postorder(Node *root) {
   if (root != NULL) {   
      postorder(root->GetLeft());
      postorder(root->GetRight());
      cout<<root->GetLabel()<<" "<<endl;
   }
}

void preorder(Node *root) {
   if (root != NULL) {   
	  cout<<root->GetLabel()<<" "<<endl;
      postorder(root->GetLeft());
      postorder(root->GetRight());
      
   }
}

void inorder(Node *root) {
   if (root != NULL) {   
	  postorder(root->GetLeft());
	  cout<<root->GetLabel()<<" "<<endl;
      postorder(root->GetRight());
      
   }
}

void SCL_Interpreter::Start()
{
	cout << "Start" << endl;

	string Lexeme;

	unsigned int Entry = 0, LastLine = Scanner->GetLexemesAndTokenVector().front().Line;

	vector<Lexemes_and_Tokens_t> LexemesAndTokensVector = Scanner->GetLexemesAndTokenVector();
/*
	for(unsigned int Iterator = 0; Iterator < LexemesAndTokensVector.size(); ++Iterator)
	{
		Entry = LexemesAndTokensVector[Iterator].TokenEntry.token;

		Lexeme = LexemesAndTokensVector[Iterator].LexemeEntry.lexeme;

		Iterator += Dispatch (Entry, Lexeme, Iterator);
	}
*/
	
	LinkedList* head_ptr = head->GetNext();

	while (head_ptr != nullptr)
	{
		Node* data = reinterpret_cast<Node*>(head_ptr->GetData());

		if(data->GetLeft() != nullptr || data->GetRight() != nullptr) // data, but left should not be null
		{
			Dispatch(data);
		}

		head_ptr = head_ptr->GetNext();
	}
}

unsigned int SCL_Interpreter::Dispatch(Node* entry)
{
	unsigned int ReturnValue = 0;

	vector<Lexemes_and_Tokens_t> LexemesAndTokensVector = Scanner->GetLexemesAndTokenVector();

	switch (entry->GetToken())
	{
		case IMPORT:
		{
			cout << "Importing " << endl;
		}
		break;

		case IMPLEMENTATIONS:
		{
			cout << "Implementations" << endl;
		}
		break;
	}
	

	return ReturnValue;
}

void SCL_Interpreter::Finish()
{
	cout << "Finish" << endl;
}